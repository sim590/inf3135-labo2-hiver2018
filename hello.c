#include <stdio.h>

// argv: ['nom_programme', '$1', '$2', '$3', ...]

int main(int argc, char* argv[]) {
    if (argc == 2)
        printf("Hello, %s!\n", argv[1]);
    else
        printf("Erreur: le nombre d'arguments requis est 1.\n");

    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

